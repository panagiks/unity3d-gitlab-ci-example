#!/usr/bin/env bash

set -e
set -x
mkdir -p /root/.cache/unity3d
mkdir -p /root/.local/share/unity3d/Unity/
set +x
echo 'Writing $UNITY_LICENSE_CONTENT to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf'
if [ $BUILD_TARGET = "iOS" ]; then
    license=$UNITY_IOS_LICENSE;
elif [ $BUILD_TARGET = "Android" ]; then
    license=$UNITY_ANDROID_LICENSE;
else
    license=$UNITY_LICENSE_CONTENT;
fi

echo "$license" | tr -d '\r' > /root/.local/share/unity3d/Unity/Unity_lic.ulf